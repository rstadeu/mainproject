import { combineReducers } from 'redux'
import {movies, moviesByDirector, activeDirector } from './Movies'
import ActiveMovieReducer from './ActiveMovie'
import {search_criteria, sort_criteria, search_active_criteria, sort_active_criteria} from './Criterias'
import keyword from './Keyword'


const allReducers = combineReducers({
  movies,
  moviesByDirector,
  selectedMovie: ActiveMovieReducer,
  search_criteria,
  sort_criteria,
  search_active_criteria,
  sort_active_criteria,
  keyword,
  activeDirector
});

export default allReducers;