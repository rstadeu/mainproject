const Dates = {};

Dates.dateStringToYear = dateString => {
  const date = new Date(dateString);
  return date.getFullYear();
};

Dates.getRandom = (from, to) => {
  return Math.round(Math.random() * (to - from) + to)
};

Dates.mapWithUniqueId = (movies) => {
  return movies.map(movie=>{
    movie.c_id = Dates.getRandom(99, 9999) + movie.id;
    return movie;
  });
}

Dates.sortMovies = (movies, option) => {
  const sortedMovies = movies.sort((a,b) => {
    let aVal, bVal;
    switch (option.prop) {
      case 'vote_average':
        aVal = parseFloat(a[option.prop]);
        bVal =  parseFloat(b[option.prop]);
        break;
      case 'release_date':
        aVal = Dates.dateStringToYear(a[option.prop]);
        bVal = Dates.dateStringToYear(b[option.prop]);
        break;
    }
    if (aVal === bVal) {
      return a.id < b.id ? -1 : 1;
    } else if (aVal < bVal) {
      return 1;
    } else {
      return -1;
    }
  });
  
  return Dates.mapWithUniqueId(sortedMovies);
};

Dates.removeDuplicatesByProperty = (movies, prop) => {
  let ids = [];
  return movies.filter(el => {
    if (ids.indexOf(el[prop]) === -1) {
      ids.push(el[prop]);
      return true;
    }
  })
}

Dates.findDirector = (crewList, isAll) => {
  const directorsResult = crewList.filter(el => el.job === 'Director');
  return directorsResult.length > 0 ? isAll === true ? directorsResult : directorsResult[0] : {}
}

export default Dates;
