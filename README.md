# Task4 (Testing lecture)

Coverage > 60% Write tests using enzyme and jest

Use snapshot testing

Use coverage tool

Write at least one e2e test using library from the list: Cypress, CasperJS, Protractor, Nightwatch, Webdriver

Evaluation criteria:

(each mark includes previous mark criteria)

|                   2                  |                       3                       |                          4                          |                                  5                                  |
|:------------------------------------:|:---------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------------------------:|
| Coverage > 60% | Use snapshot testing | Coverage > 80% Functional testing with enzyme and jest | write at least one e2e test |

![TestingData](public/img/TestReport.jpg)

# Task 5 (Flux + Redux) 


Go through API docs in swagger: https://reactjs-cdp.herokuapp.com/api-docs API Endpoint: https://reactjs-cdp.herokuapp.com/

Make your components perform real AJAX requests.

Move data fetches to actions and pass data to your components with redux.

Cover actions and reducers with unit tests.

Add the ability to store your apps state offline and use it to start-up the app. You can take a look at redux-persist library for further reference.

Evaluation criteria:

(each mark includes previous mark criteria)

|                   2                  |                       3                       |                          4                          |                                  5                                  |
|:------------------------------------:|:---------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------------------------:|
| All data fetches moved to actions & received from store by components | Filtering and sorting is done as redux actions | Actions and reducers covered with unit tests (~60%+, can be amended by mentor) | Offline data storage & store restoration (coverage ~100%) |

## Running the project

Install all dependencies
```
npm install
```

To Run 
```
npm run start
```

To Build 
```
npm run build
```

To Run Tests
```
npm run test
```

## Captures from the actual Project
Main Page


![MainScreen](public/img/PageOne.jpg)

Search by Title


![SearchMovie](public/img/PageTwo.jpg)

Search by Director


![SearchDirector](public/img/PageThree.jpg)
